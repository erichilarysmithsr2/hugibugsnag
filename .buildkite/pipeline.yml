agents:
  queue: macos-12-arm

steps:
  #
  # Build Plugins
  #

  # UE 4.23
  - label: 'Build Plugin - 4.23 Mac'
    agents:
      queue: opensource-mac-cocoa-10.15
    env:
      UE_VERSION: "4.23"
      DEVELOPER_DIR: "/Applications/Xcode11.app"
      NDKROOT: /Users/administrator/Library/Android/sdk/ndk/16.1.4479499
    commands:
      - rm -rf "/Users/administrator/Library/Logs/Unreal Engine/LocalBuildLogs/*"
      - make package
    plugins:
      artifacts#v1.5.0:
        upload:
          - "/Users/administrator/Library/Logs/Unreal Engine/LocalBuildLogs/*"
    artifact_paths: [Build/Plugin/*.zip]
    timeout_in_minutes: 60
    key: plugin_4_23

  # UE 4.27
  - label: 'Build Plugin - 4.27 Mac'
    env:
      UE_VERSION: "4.27"
      DEVELOPER_DIR: "/Applications/Xcode13.2.1.app"
    commands:
      - rm -rf "/Users/administrator/Library/Logs/Unreal Engine/LocalBuildLogs/*"
      - make package
    plugins:
      artifacts#v1.5.0:
        upload:
          - "/Users/administrator/Library/Logs/Unreal Engine/LocalBuildLogs/*"
    artifact_paths: [Build/Plugin/*.zip]
    timeout_in_minutes: 60
    key: plugin_4_27

  # Unreal Engine 4.27 - Windows
  - label: 'Build Plugin - 4.27 Win'
    agents:
      queue: windows-general-wsl
    env:
      UE_VERSION: "4.27"
    command: features/scripts/build-plugin-wsl.sh
    artifact_paths: [Bugsnag-*.zip]
    timeout_in_minutes: 60

  # Unreal Engine 5.1 - Windows
  - label: 'Build Plugin - 5.1 Win'
    agents:
      queue: windows-general-wsl
    env:
      UE_VERSION: "5.1"
    command: features/scripts/build-plugin-wsl.sh
    artifact_paths: [Bugsnag-*.zip]
    timeout_in_minutes: 60

  #
  # Build Test Fixtures
  #

  # UE 4.27
  - name: ':android: Build E2E - 4.27 Android'
    depends_on: plugin_4_27
    env:
      UE_VERSION: "4.27"
      DEVELOPER_DIR: "/Applications/Xcode13.2.1.app"
    plugins:
      artifacts#v1.5.0:
        download: Build/Plugin/Bugsnag-*-UE_4.27-macOS.zip
        upload:
          - build/TestFixture-Android-Shipping-4.27-arm64.apk
          - build/TestFixture-Android-Shipping-4.27-armv7.apk
          - "/Users/administrator/Library/Logs/Unreal Engine/LocalBuildLogs/*"
    commands:
      - rm -rf "/Users/administrator/Library/Logs/Unreal Engine/LocalBuildLogs/*"
      - features/scripts/build-fixture.sh Android
    timeout_in_minutes: 60
    key: android_fixture_4_27

  - name: ':ios: Build E2E - 4.27 iOS'
    depends_on: plugin_4_27
    env:
      UE_VERSION: "4.27"
      DEVELOPER_DIR: "/Applications/Xcode13.2.1.app"
    plugins:
      artifacts#v1.5.0:
        download: Build/Plugin/Bugsnag-*-UE_4.27-macOS.zip
        upload:
          - build/TestFixture-IOS-Shipping-4.27.dSYM
          - build/TestFixture-IOS-Shipping-4.27.ipa
          - "/Users/administrator/Library/Logs/Unreal Engine/LocalBuildLogs/*"
    commands:
      - rm -rf "/Users/administrator/Library/Logs/Unreal Engine/LocalBuildLogs/*"
      - features/scripts/build-fixture.sh IOS
    timeout_in_minutes: 60
    key: ios_fixture_4_27

  - name: ':mac: Build E2E - 4.27 Mac'
    depends_on: plugin_4_27
    env:
      UE_VERSION: "4.27"
      DEVELOPER_DIR: "/Applications/Xcode13.2.1.app"
    plugins:
      artifacts#v1.5.0:
        download: Build/Plugin/Bugsnag-*-UE_4.27-macOS.zip
        upload:
          - TestFixture-macOS-4.27.zip
          - "/Users/administrator/Library/Logs/Unreal Engine/LocalBuildLogs/*"
    commands:
      - rm -rf "/Users/administrator/Library/Logs/Unreal Engine/LocalBuildLogs/*"
      - features/scripts/build-fixture.sh Mac
    timeout_in_minutes: 90
    key: mac_fixture_4_27

  #
  # E2E Tests
  #

  # UE 4.27
  - label: 'E2E Tests - 4.27 Android 11'
    depends_on: android_fixture_4_27
    timeout_in_minutes: 30
    agents:
      queue: opensource
    plugins:
      artifacts#v1.3.0:
        download:
            - build/TestFixture-Android-Shipping-4.27-arm64.apk
        upload: ["maze_output/failed/**/*"]
      docker-compose#v3.3.0:
        run: maze-runner
        command:
          - "--app=/app/build/TestFixture-Android-Shipping-4.27-arm64.apk"
          - "--appium-version=1.17.0"
          - "--device=ANDROID_11_0"
          - "--farm=bs"
          - "--order=random"
    concurrency: 24
    concurrency_group: browserstack-app
    concurrency_method: eager
    retry:
      automatic:
        - exit_status: -1  # Agent was lost
          limit: 2

  - label: 'E2E Tests - 4.27 iOS 12'
    depends_on: ios_fixture_4_27
    timeout_in_minutes: 30
    agents:
      queue: opensource
    plugins:
      artifacts#v1.3.0:
        download:
            - build/TestFixture-IOS-Shipping-4.27.ipa
            - build/TestFixture-IOS-Shipping-4.27.dSYM
        upload: ["maze_output/failed/**/*"]
      docker-compose#v3.3.0:
        run: maze-runner
        command:
          - "--app=/app/build/TestFixture-IOS-Shipping-4.27.ipa"
          - "--appium-version=1.17.0"
          - "--device=IOS_12"
          - "--farm=bs"
          - "--order=random"
    concurrency: 24
    concurrency_group: browserstack-app
    concurrency_method: eager
    retry:
      automatic:
        - exit_status: -1  # Agent was lost
          limit: 2

  - label: 'E2E Tests - 4.27 macOS 12'
    depends_on: mac_fixture_4_27
    timeout_in_minutes: 10
    plugins:
      artifacts#v1.5.0:
        download:
          -  TestFixture-macOS-4.27.zip
        upload: ["maze_output/failed/**/*"]
    commands:
      - echo '--- Extracting test fixture'
      - unzip TestFixture-macOS-4.27.zip
      - echo '--- Installing dependencies'
      - bundle install
      - echo '--- Running tests'
      - bundle exec maze-runner --os=macos

  #
  # Conditionally trigger full pipeline
  #
  - label: 'Conditionally trigger full set of tests'
    command: sh -c .buildkite/pipeline_trigger.sh
